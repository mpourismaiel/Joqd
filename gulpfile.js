const gulp = require('gulp')
const del = require('del')
const sass = require('gulp-ruby-sass')

gulp.task('del:sass', function () {
  return del(['./dest/css/**/*'])
})

gulp.task('del:js', function () {
  return del(['./dest/js/**/*'])
})

gulp.task('sass', ['del:sass'], function (){
  sass('./src/scss/entry.scss', { style: 'expanded' })
    .pipe(gulp.dest('./dest/css'))
})

gulp.task('js', ['del:js'], function () {
  gulp.src('./src/js/**/*.js')
    .pipe(gulp.dest('./dest'))
})

gulp.task('watch', function (){
  gulp.watch('./src/scss/**/*.scss', ['sass'])
  gulp.watch('./src/**/*.js', ['js'])
})

gulp.task('default', ['sass', 'js', 'watch'])
